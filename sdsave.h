#pragma once
#include <stdint.h>

#include "Pokitto.h"
#ifdef POK_SIM
#include "io.h"
#else
#include "SDFileSystem.h"
#include "FATFileHandle.h"
#endif

void initsd();
void closesd();
void initrooms();
void loadenemiesroom(uint8_t r,uint16_t enemiesdata[7][15]);
void saveroom(uint8_t r,uint16_t roomdata[26][35]);
void loadroom(uint8_t r,uint16_t roomdata[26][35]);
