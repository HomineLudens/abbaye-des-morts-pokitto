/**************************************************************************/
/*!
    @file     My_settings.h
    @author   XX

    @section HOW TO USE My_settings

   My_settings can be used to set project settings inside the mbed online IDE
*/
/**************************************************************************/

#pragma once

#define DISABLEAVRMIN

#define PROJ_MODE15 	  1     // 220x176 hires 16 color mode
#define PROJ_ENABLE_SOUND 1     // 0 = all sound functions disabled
#define PROJ_STREAMING_MUSIC 1
#define PROJ_HIGH_RAM HIGH_RAM_MUSIC // put music buffers in SRAM1/2
#define PROJ_SDFS_STREAMING

#define PROJ_AUD_FREQ 8000
// #define PROJ_DEVELOPER_MODE 1
#define USE_HIGH_COLOR



